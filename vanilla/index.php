<!DOCTYPE html>
<html>
<head>
	<title>Vanilla PHP Sockets Chat</title>
	<link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
	<div class="chat">
		<h1>Vanilla PHP WebSockets Chat</h1>
		<div class="box" id="box"></div>

		<div class="panel">
			<input type="text" name="name" id="name" placeholder="Your Name" maxlength="10" style="width:20%"  />
			<input type="text" name="message" id="message" placeholder="Message" maxlength="80" style="width:60%" />

			<button id="send">Send</button>
		</div>
	</div>

	<script src="js/jquery.min.js"></script>
	<script src="/js/main.js" type="text/javascript"></script>
</body>
</html>