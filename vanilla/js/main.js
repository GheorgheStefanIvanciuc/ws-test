$(document).ready(function() {
  var colours = Array('007AFF','FF7000','FF7000','15E25F','CFC700', 'CFC700','CF1100','CF00BE','F00');
  var crtColor = colours[Math.floor(Math.random() * colours.length)];

  var wsUri = "ws://46.101.146.48:1301/server.php";  
  websocket = new WebSocket(wsUri); 
  
  websocket.onopen = function(e) {
    $('#box').append("<div class=\"notice\">Connected!</div>");
  }

  websocket.onmessage = function(e) {
    var msg = JSON.parse(e.data);

    if(msg.type == 'usermsg') {
      $('#box').append("<div><span class=\"name\" style=\"color:#"+msg.color+"\">"+msg.name+"</span> : <span class=\"message\">"+msg.message+"</span></div>");
    }

    if(msg.type == 'notice') {
      $('#box').append("<div class=\"notice\">"+msg.message+"</div>");
    }
    
    $('#message').val('');
  };

  websocket.onerror = function(e) {
    $('#box').append("<div class=\"notice\">Error Occurred - "+e.data+"</div>");
  }; 
 
  websocket.onclose = function(e) {
    $('#box').append("<div class=\"notice\">Connection Closed</div>");
  }; 

  $('#send').click(function() {  
    var text = $('#message').val();
    var name = $('#name').val();
    
    if(name == "") {
      alert("Please enter a name!");
      return;
    }

    if(text == "") {
      alert("Please type a message first!");
      return;
    }
    
    $('#name').attr('disabled', 'disabled');

    var msg = {
      message: text,
      name: name,
      color : crtColor
    };

    websocket.send(JSON.stringify(msg));
  });
});