<?php

	$host = 'localhost';
	$port = '1301';
	$null = NULL;


	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

	socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
	socket_bind($socket, 0, $port);
	socket_listen($socket);

	$clients = array($socket);

	while (true)
	{
		$changed = $clients;
		socket_select($changed, $null, $null, 0, 10);
		
		if (in_array($socket, $changed))
		{
			$newSocket = socket_accept($socket);
			$clients[] = $newSocket;
			
			$header = socket_read($newSocket, 1024);
			performHandshaking($header, $newSocket, $host, $port);
			
			socket_getpeername($newSocket, $ip);
			$response = mask(json_encode(array('type' => 'notice', 'message' => $ip . ' connected')));
			send_message($response);
			
			$oldSocket = array_search($socket, $changed);
			unset($changed[$oldSocket]);
		}
		
		foreach ($changed as $changedSocket)
		{	
			
			while(socket_recv($changedSocket, $buf, 1024, 0) >= 1)
			{
				$received_text = unmask($buf);
				$tst_msg = json_decode($received_text);
				$user_name = $tst_msg->name;
				$user_message = $tst_msg->message;
				$user_color = $tst_msg->color;
				
				if (!is_null($user_name))
				{
					$response_text = mask(json_encode(array('type' => 'usermsg', 'name' => $user_name, 'message' => $user_message, 'color'=>$user_color)));
					send_message($response_text);
				}
				
				break 2;
			}
			
			$buf = @socket_read($changedSocket, 1024, PHP_NORMAL_READ);
			if ($buf === false)
			{
				$found_socket = array_search($changedSocket, $clients);
				socket_getpeername($changedSocket, $ip);
				unset($clients[$found_socket]);
				
				$response = mask(json_encode(array('type' => 'notice', 'message' => $ip . ' disconnected')));
				send_message($response);
			}
		}
	}

	socket_close($socket);

	function send_message($msg)
	{
		global $clients;

		foreach($clients as $clientSocket)
			@socket_write($clientSocket, $msg, strlen($msg));

		return true;
	}

	function unmask($text)
	{
		$length = ord($text[1]) & 127;

		if($length == 126)
		{
			$masks = substr($text, 4, 4);
			$data = substr($text, 8);
		}
		elseif($length == 127)
		{
			$masks = substr($text, 10, 4);
			$data = substr($text, 14);
		}
		else
		{
			$masks = substr($text, 2, 4);
			$data = substr($text, 6);
		}

		$text = "";
		for ($i = 0; $i < strlen($data); ++$i)
			$text .= $data[$i] ^ $masks[$i%4];

		return $text;
	}

	function mask($text)
	{
		$b1 = 0x80 | (0x1 & 0x0f);
		$length = strlen($text);
		
		if($length <= 125)
			$header = pack('CC', $b1, $length);
		elseif($length > 125 && $length < 65536)
			$header = pack('CCn', $b1, 126, $length);
		elseif($length >= 65536)
			$header = pack('CCNN', $b1, 127, $length);
		return $header.$text;
	}

	function performHandshaking($receved_header,$client_conn, $host, $port)
	{
		$headers = array();
		$lines = preg_split("/\r\n/", $receved_header);
		foreach($lines as $line)
		{
			$line = chop($line);
			if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
			{
				$headers[$matches[1]] = $matches[2];
			}
		}

		$secKey = isset($headers['Sec-WebSocket-Key']) ? $headers['Sec-WebSocket-Key'] : '';
		$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
		//hand shaking header
		$upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
		"Upgrade: websocket\r\n" .
		"Connection: Upgrade\r\n" .
		"WebSocket-Origin: $host\r\n" .
		"WebSocket-Location: ws://$host:$port/server.php\r\n".
		"Sec-WebSocket-Accept:$secAccept\r\n\r\n";
		socket_write($client_conn,$upgrade,strlen($upgrade));
	}
