var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use('/js', express.static(__dirname + '/js'));
app.use('/css', express.static(__dirname + '/css'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

http.listen(1313, function() {
  console.log('listening on *:1313');
});

io.on('connection', function (socket) {
  socket.emit('open');
  socket.broadcast.emit('message', {
    type: 'notice',
    message: socket.handshake.address + ' connected'
  });

  socket.on('message', function (data) {
    io.emit('message', {
      type: 'usermsg',
      name: data.name,
      message: data.message,
      color: data.color
    });
  });

  socket.on('error', function () {
    socket.emit('error');
  });

  socket.on('disconnect', function () {
    socket.emit('close');
    socket.broadcast.emit('message', {
      type: 'notice',
      message: socket.handshake.address + ' disconnected'
    });
  });
});

