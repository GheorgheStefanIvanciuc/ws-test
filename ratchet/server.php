<?php

require_once dirname(__DIR__) . '/ratchet/vendor/autoload.php';
require_once 'src/Chat.php';

use App\Chat;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;


    

    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        1302
    );

    $server->run();