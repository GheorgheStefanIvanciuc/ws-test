<?php namespace App;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface
{
	protected $clients;

	public function __construct()
	{
		$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn)
	{
		$this->clients->attach($conn);

		foreach ($this->clients as $client)
			if ($conn !== $client)
				$client->send(json_encode(array('type' => 'notice', 'message' => $conn->remoteAddress . ' connected')));
	}

	public function onMessage(ConnectionInterface $from, $msg)
	{
		$msg = json_decode($msg);

		if (!is_null($msg->name))
		{
			$response = json_encode(array('type' => 'usermsg', 'name' => $msg->name, 'message' => $msg->message, 'color'=> $msg->color));
			
			foreach ($this->clients as $client)
				$client->send($response);
		}

	}

	public function onClose(ConnectionInterface $conn)
	{
		$this->clients->detach($conn);

		foreach ($this->clients as $client)
			if ($conn !== $client)
				$client->send(json_encode(array('type' => 'notice', 'message' => $conn->remoteAddress . ' disconnected')));
	}

	public function onError(ConnectionInterface $conn, \Exception $e)
	{
		foreach ($this->clients as $client)
			$client->send(json_encode("An error has occurred: {$e->getMessage()}\n"));

		$conn->close();
	}
}